package ru.t1.stepanishchev.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.stepanishchev.tm.api.endpoint.ISystemEndpoint;
import ru.t1.stepanishchev.tm.api.service.IPropertyService;
import ru.t1.stepanishchev.tm.dto.request.ServerAboutRequest;
import ru.t1.stepanishchev.tm.dto.request.ServerVersionRequest;
import ru.t1.stepanishchev.tm.dto.response.ServerAboutResponse;
import ru.t1.stepanishchev.tm.dto.response.ServerVersionResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@NoArgsConstructor
@AllArgsConstructor
@WebService(endpointInterface = "ru.t1.stepanishchev.tm.api.endpoint.ISystemEndpoint")
public final class SystemEndpoint extends AbstractEndpoint implements ISystemEndpoint {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Override
    @WebMethod
    public ServerAboutResponse getAbout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerAboutRequest request
    ) {
        @NotNull final ServerAboutResponse response = new ServerAboutResponse();
        response.setEmail(propertyService.getAuthorEmail());
        response.setName(propertyService.getAuthorName());
        return response;
    }

    @NotNull
    @Override
    @WebMethod
    public ServerVersionResponse getVersion(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerVersionRequest request
    ) {
        @NotNull final ServerVersionResponse response = new ServerVersionResponse();
        response.setVersion(propertyService.getApplicationVersion());
        return response;
    }

}
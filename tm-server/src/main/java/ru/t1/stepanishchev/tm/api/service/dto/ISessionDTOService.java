package ru.t1.stepanishchev.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.stepanishchev.tm.dto.model.SessionDTO;

@Service
public interface ISessionDTOService extends IUserOwnedDTOService<SessionDTO> {

    @NotNull
    SessionDTO create(@Nullable SessionDTO session);

    Boolean existsById(@Nullable String id);

    @Nullable
    SessionDTO findOneById(@Nullable String id);

    @NotNull
    SessionDTO removeOneById(@Nullable String id);

    void clear(@NotNull String userId);

}
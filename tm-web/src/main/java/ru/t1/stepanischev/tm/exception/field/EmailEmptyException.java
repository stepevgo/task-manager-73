package ru.t1.stepanischev.tm.exception.field;

public class EmailEmptyException extends AbstractFieldException {

    public EmailEmptyException() {
        super("Error! Email is empty...");
    }

}
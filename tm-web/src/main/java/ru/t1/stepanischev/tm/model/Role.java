package ru.t1.stepanischev.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanischev.tm.enumerated.RoleType;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Getter
@Setter
@Table(name = "tm_role")
public class Role {

    @Id
    @Column
    @NotNull
    private String id = UUID.randomUUID().toString();

    @Nullable
    @JoinColumn(name = "user_id")
    @ManyToOne
    private User user;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "role_type")
    private RoleType roleType = RoleType.USER;

    @NotNull
    @Override
    public String toString() {
        return roleType.name();
    }

}

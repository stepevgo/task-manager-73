package ru.t1.stepanishchev.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.stepanishchev.tm.api.service.IPropertyService;
import ru.t1.stepanishchev.tm.listener.AbstractListener;
import ru.t1.stepanishchev.tm.enumerated.Role;

@Component
public abstract class AbstractSystemListener extends AbstractListener {

    @Autowired
    protected AbstractListener[] listeners;

    @NotNull
    @Autowired
    public IPropertyService propertyService;

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}